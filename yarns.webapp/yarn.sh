# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# =*= License: GPL-2 =*=

# This file is a yarn shell library for testing Lorry Controller.


# Kill a daemon given its pid file. Report whether it got killed or not.

kill_daemon_using_pid_file()
{
    local pid=$(head -n1 "$1")
    if kill -9 "$pid"
    then
        echo "Killed daemon running as $pid"
    else
        echo "Error killing daemon running as pid $pid"
    fi
}


# Add a configuration item to a cliapp-style configuration file.

add_to_config_file()
{
    if [ ! -e "$1" ]
    then
        printf '[config]\n' > "$1"
    fi
    printf '%s = %s\n' "$2" "$3" >> "$1"
}


# Ensure the standard input is empty. If not, exit with an error.

stdin_is_empty()
{
    if grep . > /dev/null
    then
        echo "ERROR: stdin was NOT empty" 1>&2
        exit 1
    fi
}


# Configure (unless already configured) and start a WEBAPP.

start_webapp()
{
    rm -f "$DATADIR/webapp.pid"
    rm -f "$DATADIR/webapp.port"
    mkfifo "$DATADIR/webapp.port"

    add_to_config_file "$DATADIR/webapp.conf" \
        statedb "$DATADIR/webapp.db"
    add_to_config_file "$DATADIR/webapp.conf" \
        status-html "$DATADIR/lc-status.html"
    add_to_config_file "$DATADIR/webapp.conf" \
        log "$DATADIR/webapp.log"
    add_to_config_file "$DATADIR/webapp.conf" \
        log-level debug
    add_to_config_file "$DATADIR/webapp.conf" \
        debug-host 127.0.0.1
    add_to_config_file "$DATADIR/webapp.conf" \
        debug-port-file "$DATADIR/webapp.port"
    add_to_config_file "$DATADIR/webapp.conf" \
        static-files "$SRCDIR/static"
    add_to_config_file "$DATADIR/webapp.conf" \
       templates "$SRCDIR/templates"
    add_to_config_file "$DATADIR/webapp.conf" \
        debug-real-confgit no

    start-stop-daemon -S -x "$SRCDIR/lorry-controller-webapp" \
        -b -p "$DATADIR/webapp.pid" -m --verbose \
        -- \
        --config "$DATADIR/webapp.conf"

    port=$(cat "$DATADIR/webapp.port")
    rm -f "$DATADIR/webapp.port"
    echo "$port" >"$DATADIR/webapp.port"

    # Wait for the WEBAPP to actually be ready, i.e., that it's
    # listening on its assigned port.
    "$SRCDIR/test-wait-for-port" 127.0.0.1 "$port"
}


# Make a POST request.

post_request()
{
    > "$DATADIR/response.headers"
    > "$DATADIR/response.body"
    port=$(cat "$DATADIR/webapp.port")

    # The timestamp is needed by "THEN static status page got updated"
    touch "$DATADIR/request.timestamp"

    curl \
        -D "$DATADIR/response.headers" \
        -o "$DATADIR/response.body" \
        --silent --show-error \
        --request POST \
        --data "$2" \
        "http://127.0.0.1:$port$1"
    cat "$DATADIR/response.headers"
    cat "$DATADIR/response.body"
    head -n1 "$DATADIR/response.headers" | grep '^HTTP/1\.[01] 200 '
}
