# Copyright (C) 2015  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import cliapp


class Gerrit(object):

    '''Run commands on a Gerrit instance.

    This uses the SSH API to Gerrit. The REST API is actually much nicer to
    use, but it requires that the account doing stuff has set an HTTP password.
    Use of the SSH API requires only the SSH key that we also use for push
    access.

    '''

    def __init__(self, host, user, port=29418):
        self._ssh_command_args = [
            'ssh', '-oStrictHostKeyChecking=no', '-oBatchMode=yes', '-p%i' % port,
            '%s@%s' % (user, host)]

    def _ssh_command(self, command):
        return cliapp.runcmd(self._ssh_command_args + command)

    def has_project(self, name):
        # There's no 'does this project exist' command in Gerrit 2.9.4; 'list
        # all projects with this prefix' is as close we can get.

        output = self._ssh_command([
            'gerrit', 'ls-projects', '--type=ALL', '--prefix=%s' % name])
        projects = output.strip().split('\n')

        if name in projects:
            return True
        else:
            return False

    def create_project(self, name):
        self._ssh_command(['gerrit', 'create-project', name])
