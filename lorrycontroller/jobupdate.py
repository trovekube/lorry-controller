# Copyright (C) 2014-2017  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import logging
import time

import bottle

import lorrycontroller


class JobUpdate(lorrycontroller.LorryControllerRoute):

    http_method = 'POST'
    path = '/1.0/job-update'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)

        job_id = int(bottle.request.forms.job_id)
        exit = bottle.request.forms.exit
        stdout = bottle.request.forms.stdout
        stderr = bottle.request.forms.stderr
        disk_usage = bottle.request.forms.disk_usage

        logging.info('Job %s updated (exit=%s)', job_id, exit)

        with self.open_statedb() as statedb:
            if stdout:
                statedb.append_to_job_output(job_id, stdout)
            if stderr:
                statedb.append_to_job_output(job_id, stderr)

            now = statedb.get_current_time()
            statedb.set_job_updated(job_id, now)

            path = statedb.find_lorry_running_job(job_id)
            lorry_info = statedb.get_lorry_info(path)

            if exit is not None and exit != 'no':
                if exit != '0':
                    job_output = statedb.get_job_output(job_id)
                else:
                    job_output = ''
                statedb.set_lorry_last_run_exit_and_output(path, exit, job_output)
                statedb.set_lorry_last_run(path, int(now))
                statedb.set_running_job(path, None)
                statedb.set_job_exit(job_id, exit, int(now), disk_usage)
                statedb.set_lorry_disk_usage(path, disk_usage)
            elif self.time_to_die(statedb, job_id, lorry_info):
                logging.warning(
                    'Job %r has been running too long, '
                    'marking it to be exterminated', job_id)
                statedb.set_kill_job(job_id, True)

            obj = statedb.get_job_info(job_id)
            logging.debug('obj=%r', obj)
            return obj

    def time_to_die(self, statedb, job_id, lorry_info):
        started, ended = statedb.get_job_started_and_ended(job_id)
        lorry_timeout = lorry_info['lorry_timeout']
        now = statedb.get_current_time()
        age = now - started
        logging.debug('started=%r', started)
        logging.debug('ended=%r', ended)
        logging.debug('lorry_timeout=%r', lorry_timeout)
        logging.debug('now=%r', now)
        logging.debug('age=%r', age)
        return age >= lorry_timeout
