# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import json
import os
import urllib


def build_proxy_url(protocol, proxy_config):
    """Build a proxy URL from data in our proxy configuration format."""

    hostname = urllib.quote(proxy_config['hostname'])
    url = '%s:%s' % (hostname, proxy_config['port'])

    if 'username' not in proxy_config:
        return '%s://%s/' % (protocol, url)
    elif 'password' not in proxy_config:
        return '%s://%s@%s/' % (protocol, proxy_config['username'], url)
    else:
        return '%s://%s:%s@%s/' % (protocol, proxy_config['username'],
                                   proxy_config['password'], url)


def setup_proxy(config_filename):
    """Tell urllib2 to use a proxy for http action by lorry-controller.

    Load the proxy information from the JSON file given by proxy_def, then
    set urllib2's url opener to open urls via an authenticated proxy.

    """

    if not os.path.exists(config_filename):
        return

    with open(config_filename, 'r') as f:
        proxy_config = json.load(f)

    http_proxy_url = build_proxy_url('http', proxy_config)
    https_proxy_url = build_proxy_url('https', proxy_config)

    # set the required environment variables
    os.environ['http_proxy'] = http_proxy_url
    os.environ['https_proxy'] = https_proxy_url
