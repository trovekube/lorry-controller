# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import logging

import bottle

import lorrycontroller


class StopJob(lorrycontroller.LorryControllerRoute):

    http_method = 'POST'
    path = '/1.0/stop-job'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)
        job_id = bottle.request.forms.job_id
        statedb = self.open_statedb()
        with statedb:
            try:
                path = statedb.find_lorry_running_job(job_id)
            except lorrycontroller.WrongNumberLorriesRunningJob:
                logging.warning(
                    "Tried to kill job %s which isn't running" % job_id)
                bottle.abort(409, 'Job is not currently running')
            statedb.set_kill_job(job_id, True)
        return statedb.get_job_info(job_id)
