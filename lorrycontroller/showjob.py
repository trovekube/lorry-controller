# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import logging
import time

import bottle

import lorrycontroller


class JobShower(object):

    def get_job_as_json(self, statedb, job_id):
        path = statedb.get_job_path(job_id)
        exit = statedb.get_job_exit(job_id)
        output = statedb.get_job_output(job_id)
        started, ended = statedb.get_job_started_and_ended(job_id)
        disk_usage = statedb.get_job_disk_usage(job_id)
        now = statedb.get_current_time()

        return {
            'job_id': job_id,
            'host': statedb.get_job_minion_host(job_id),
            'pid': statedb.get_job_minion_pid(job_id),
            'path': statedb.get_job_path(job_id),
            'exit': 'no' if exit is None else exit,
            'disk_usage': disk_usage,
            'disk_usage_nice': self.format_bytesize(disk_usage or 0),
            'output': output,
            'job_started': self.format_time(started),
            'job_ended': '' if ended is None else self.format_time(ended),
            'timestamp': self.format_time(now),
            }

    def format_time(self, timestamp):
        return time.strftime('%Y-%m-%d %H:%M:%S UTC', time.gmtime(timestamp))

    def format_bytesize(self, num_bytes):
        if num_bytes is None:
            return 'unknown'
        mebibyte = 2**20
        return '%.1f MiB' % (float(num_bytes) / float(mebibyte))


class ShowJob(lorrycontroller.LorryControllerRoute):

    http_method = 'GET'
    path = '/1.0/job/<job_id:int>'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)
        job_id = int(kwargs['job_id'])

        statedb = self.open_statedb()
        return JobShower().get_job_as_json(statedb, job_id)


class ShowJobHTML(lorrycontroller.LorryControllerRoute):

    http_method = 'GET'
    path = '/1.0/job-html/<job_id:int>'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)
        job_id = int(kwargs['job_id'])

        statedb = self.open_statedb()
        variables = JobShower().get_job_as_json(statedb, job_id)
        return bottle.template(self._templates['job'], **variables)
